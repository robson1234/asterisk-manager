#!/bin/bash

###################### astManager_1-0.sh
#	Autor		:Robson Correa <robsonc.leite94@gmail.com>
#	Descricao	:Shell Script com função de auxiliar analistas na administração de servidores VOIP Asterisk
#


##################### Declaração dos comando utilizados no shell 
##################### TODO verificar se o caminho dos comandos está correto com o comando which, Script escrito no CentOS 7
ECHO="/usr/bin/echo"
REBOOT="/usr/sbin/reboot"
TOP="/usr/bin/top"
PING="/usr/bin/ping"
ASTERISK="/usr/sbin/asterisk"
LESS="/usr/bin/less"
CAT="/usr/bin/cat"
WHOAMI=`/usr/bin/whoami`
DATA=`date +%D`
HORA=`date +%T`
CUT="/usr/bin/cut"
MKDIR="/usr/bin/mkdir"
DIRLOG="/var/log/shell"
HEAD="/usr/bin/head"





##################### Declaração das funçoes do Script
##################### Função "processos" responsável por exibir os processos e carga sobre a maquina
function processos(){
	PROCESSOS=`$TOP "-n1"`
}

#################### Função "navegação " responsável por verificar conexão do server com Internet
function navegacao(){
	NAVEGACAO=`ping -c1 "8.8.8.8" | grep "Unreachable" | $CUT -b "48-58"`
	if [[ $NAVEGACAO -eq "Unreachable" ]]; then
		$ECHO "Sem conexão com a Internet"
	else
		$ECHO "Conectado a Internet"
	fi
}

################### Função "filas" responsável por exibir as filas e status dos ramais 
function filas(){
	FILAS=`$ASTERISK -rx "queue show"`
	$ECHO "$FILAS"
}

################### Função "recarregar" responsável por recarregar o asterisk
function recarregar(){
	RECARREGAR=`$ASTERISK -rx "reload"`
	$RECARREGAR
}

################## Função "logs" responsável por exibir os logs do asterisk
function logs(){
	LOGS=`$CAT "/var/log/asterisk/full" | $LESS`
}

################# Função "registro" responsável por verificar o status dos troncos no campo registry
function registro(){
	REGISTRO=`$ASTERISK -rx "sip show registry" | grep "Request" | $HEAD n1`
	if [[ $REGISTRO -eq "Request" ]]; then
		$ECHO "Troncos sem registro:"
		STATUS=`$ASTERISK -rx "sip show registry" | grep "Request"`
		$ECHO "$STATUS"
	else
		$ECHO "Todos os troncos estão registrados" 	
	fi

}

################ Função "invalida" é chamada quando é digitada uma opção inválida
function invalida(){
	$ECHO "opção invalida"
}





############### Exibição do menu de escolha do asterisk
#$ECHO "Olá, bem vindo ao gerenciador ASTERISK"
#$ECHO "por favor selecione uma das opções abaixo:"
#$ECHO "LINUX			ASTERISK		
#L1)Reinicializar	A1)Monitorar Filas
#L2)Processos		A2)Recarregar Asterisk
#L3)Navegação		A3)Verificar Logs
#L4)Realizar BKP		A4)Verificar Registros"		

############## Armazena a entrada na variável escolha
#read ESCOLHA



ESCOLHA=$( dialog --stdout \
--menu "Aplicativo Zuser - Interface amigável" \
0 0 0 \
reboot "Reinicia todo o sistema" \
processos "Abre o gerenciador de processos do sistema" \
navegacao "Verifica se o servidor tem conexão com a internet" \
filas "Abre o gerenciador de filas do sistema" \
reload "Reinicia apenas o Asterisk"
logs "abre os logs no sistema"
registro "mostra quais ramais estão registrados"
)


############## Toma decisa de acordo com o valor armazenado na variável escolha
case $ESCOLHA in
	L1 | l1 | reboot	) $REBOOT;;
	L2 | l2 | processos	) processos;;
	L3 | l3 | navegacao	) navegacao;;
	A1 | a1 | filas		) filas;;
	A2 | a2 | reload	) recarregar;;
	A3 | a3 | logs		) logs;;
	A4 | a4 | registro	) registro;;
	*) invalida;;
esac



############## Verifica se o diretório para armazenamento de logs existe e armazena logs
if [ -d $DIRLOG ]; then
	$ECHO "[$DATA $HORA]------escolha: $ESCOLHA------usuário: $WHOAMI" >> /var/log/shell/log
else
	$MKDIR $DIRLOG
	$ECHO "[$DATA $HORA]------escolha: $ESCOLHA------usuário: $WHOAMI" >> /var/log/shell/log
fi
